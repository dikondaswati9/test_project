<?php

namespace App\Http\Controllers;

use App\Status;
use App\User;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::leftJoin('status', 'status.user_id', '=', 'users.id')->get();

        return view('users.index',compact('users'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'mobile_number' => 'required|unique:users',
            'email_id' => 'required',
        ]);

        $input = $request->all();
        if ($request->hasFile('profile_pic')) {
            $file = $request->file('profile_pic');
            $file_extension = $file->getClientOriginalName();
            $destination_path = public_path() . '/images/';
            $filename = $file_extension;
            $request->file('profile_pic')->move($destination_path, $filename);
            $input['profile_pic'] = $filename;
        }


        $user = User::create($input);


        return redirect()->route('users.index')
                        ->with('success','User created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show',compact('user'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'mobile_number' => 'required|unique:users',
            'email_id' => 'required',
        ]);

        $input = $request->all();
        if ($request->hasFile('profile_pic')) {
            $file = $request->file('profile_pic');
            $file_extension = $file->getClientOriginalName();
            $destination_path = public_path() . '/images/';
            $filename = $file_extension;
            $request->file('profile_pic')->move($destination_path, $filename);
            $input['profile_pic'] = $filename;
        }


        $user->update($input);

        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }

    public function exportCsv(Request $request)
    {
        $fileName = 'users.csv';
        $users = User::all();

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('First Name', 'Last name', 'Mobile Number', 'Email Id');

        $callback = function() use($users, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($users as $user) {
                $row['First Name']  = $user->first_name;
                $row['Last Name']    = $user->last_name;
                $row['Mobile Number']    = $user->mobile_number;
                $row['Email Id']  = $user->email_id;

                fputcsv($file, array($row['First Name'], $row['Last Name'], $row['Mobile Number'], $row['Email Id']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
