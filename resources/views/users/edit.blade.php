@extends('users.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit User</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('users.update',$user->id) }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('PUT')

         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>First Name:</strong>
                    <input type="text" name="first_name" class="form-control" value ="{{$user->first_name }}" placeholder="First Name">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Last Name:</strong>
                    <input type="text" name="last_name" class="form-control" value ="{{$user->last_name}}" placeholder="Last Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Mobile Number:</strong>
                    <input type="text" name="mobile_number" class="form-control" value ="{{$user->mobile_number}}" placeholder="Mobile Number">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Email Id:</strong>
                    <input type="text" name="email_id" class="form-control" value ="{{$user->email_id}}" placeholder="Email Id">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Profile Pic:</strong>
                    <input type="file" value="{{$user->profile_pic}}" name="profile_pic" class="form-control" >
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary update-form">Submit</button>
            </div>
        </div>

    </form>

<script type="text/javascript">

  $("body").on("click",".update-form",function(e){

    $(this).parents("form").ajaxForm(options);

  });



  var options = {

    complete: function(response)

    {

    	if (response.responseJSON == undefined){

            $("input[name='first_name']").val('');
            $("input[name='last_name']").val('');
            $("input[name='email_id']").val('');
            $("input[name='mobile_number']").val('');
            $("input[name='profile_pic']").val('');

    		alert('User Update Successfully.');

    	}else{

    		printErrorMsg(response.responseJSON.errors);

    	}

    }

  };



  function printErrorMsg (msg) {

	$(".print-error-msg").find("ul").html('');

	$(".print-error-msg").css('display','block');

	$.each( msg, function( key, value ) {

		$(".print-error-msg").find("ul").append('<li>'+value+'</li>');

	});

  }

</script>
@endsection
